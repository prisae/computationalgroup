#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int **argc, char **argv)
{
	float x=5;
	float y=x;
	fprintf(stderr,"value of y = %f\n", y);

	char *s1="hello";
	char *s2=s1;

	fprintf(stderr,"%s world!\n", s1);

	/*
	char *s3, *s4;
	s3 = malloc(10*sizeof(char));
	strcpy(s3 , "hello");
	s4 = s3;
	fprintf(stderr,"%s world!\n", s4);
	free(s3);
	free(s4);
	*/
	return 0;
}
