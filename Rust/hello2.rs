fn main() {
    let x = 5;
    let y = x;
    println!("{}, number!", y);

    let s1 = String::from("hello");
    let s2 = s1;
    println!("{}, world!", s1);
    //println!("{s1}, world!");

}
